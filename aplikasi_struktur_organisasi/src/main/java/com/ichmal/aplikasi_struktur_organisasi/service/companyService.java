package com.ichmal.aplikasi_struktur_organisasi.service;

import com.ichmal.aplikasi_struktur_organisasi.model.company;
import com.ichmal.aplikasi_struktur_organisasi.repository.companyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class companyService {
    @Autowired
    private companyRepo cmpRepo;

    public List<company> listAll(){
        return cmpRepo.findAll();
    }

    public void save(company company){
        cmpRepo.save(company);
    }

    public company getId(int id){
        return cmpRepo.findById(id).get();
    }

    public void delete(int id){
        cmpRepo.deleteById(id);
    }
}
