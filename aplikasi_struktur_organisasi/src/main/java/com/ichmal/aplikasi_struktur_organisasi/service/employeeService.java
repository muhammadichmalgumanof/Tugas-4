package com.ichmal.aplikasi_struktur_organisasi.service;

import com.ichmal.aplikasi_struktur_organisasi.model.employee;
import com.ichmal.aplikasi_struktur_organisasi.repository.employeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class employeeService {
    @Autowired
    private employeeRepo empRepo;
    @Autowired
    JdbcTemplate jdbc;

    public List<employee> listAll(){
        return empRepo.findAll();
    }
    public List<employee> listTampil(){
        List<employee> listEmployee = jdbc.query(
                "SELECT e.id, e.nama, if(em.nama=e.nama, 'CEO', em.nama) as atasan, c.nama from employee e " +
                "join employee em join company c on e.company_id = c.id " +
                "where e.atasan_id = em.id or e.atasan_id is null group by e.id",
                (rs, rowNum) -> new employee(rs.getInt("e.id"), rs.getString("e.nama"),
                        rs.getString("atasan"), rs.getString("c.nama")));
        return listEmployee;
    }

    public void save(employee employee){
        empRepo.save(employee);
    }

    public employee get(int id){
        return empRepo.findById(id).get();
    }

    public void delete(int id){
        empRepo.deleteById(id);
    }
}
