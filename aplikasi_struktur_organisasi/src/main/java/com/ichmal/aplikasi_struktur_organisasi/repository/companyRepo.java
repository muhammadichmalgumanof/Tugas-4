package com.ichmal.aplikasi_struktur_organisasi.repository;

import com.ichmal.aplikasi_struktur_organisasi.model.company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface companyRepo extends JpaRepository<company, Integer> {
}
